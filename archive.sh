#!/bin/sh  



exec 2>&1 > "/tmp/build.log"

output_dir=$(mktemp -d)
open "${output_dir}"
cp -Rv "ifd-virtualserial.bundle" "${output_dir}"

cp -v "CCID/src/.libs/libccidtwin.dylib" "${output_dir}/ifd-virtualserial.bundle/Contents/MacOS/"

mkdir -p  "/usr/local/lib/pcsc/drivers/serial"

cp -Rv "${output_dir}/ifd-virtualserial.bundle" "/usr/local/lib/pcsc/drivers/serial"

open "${output_dir}"
open "/usr/local/lib/pcsc/drivers/serial"
