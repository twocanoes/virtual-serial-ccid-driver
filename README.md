## virtual-serial-ccid-driver

Virtual Serial CCID Driver is a project to trigger CryptoTokenKit drivers to virtual load identities. 


#### Building

Instructions for building virtual serial tool / CCID:

##### Install dependencies

install home brew

Install automake, pcsc-lite, pkgconfig, libtool. Makes sure to export the paths as suggested by brew.  You man need to close and open terminal window to refresh your path.

Here are the commands to run to install dependencies:

brew install automake

brew install pcsc-lite

echo 'export PATH="/opt/homebrew/opt/pcsc-lite/bin:$PATH"' >> ~/.profile

echo 'export PATH="/opt/homebrew/opt/pcsc-lite/sbin:$PATH"' >> ~/.profile

export LDFLAGS="-L/opt/homebrew/opt/pcsc-lite/lib"

export CPPFLAGS="-I/opt/homebrew/opt/pcsc-lite/include"

export PKG_CONFIG_PATH="/opt/homebrew/opt/pcsc-lite/lib/pkgconfig"

brew install pkgconfig

brew install libtool

##### Build:

./build.sh

Note: this has been tested on M1 and Intel macs and should work for both.

#### Files
/etc/reader.conf : 

This is the configuration file for non-dynamically loaded smart cards (such as via the serial port). The sample reader.conf looks like this:

-------

`FRIENDLYNAME      "TCS Virtual Serial"`

`DEVICENAME    /usr/local/lib/pcsc/drivers/serial`

`LIBPATH         /usr/local/lib/pcsc/drivers/serial/ifd-virtualserial.bundle`

`CHANNELID    1`

----
The DEVICENAME must exist since this is what lets the system know that the device is available. It can point to any directory so we point it at the serial driver since it always exists if the driver is installed.

/usr/local/lib/pcsc/drivers/serial/ifd-virtualserial.bundle:

The ifd-virtualserial.bundle is normally installed in /usr/local/lib/pcsc/drivers/serial/ifd-virtualserial.bundle and this configuration tells the system where to find it.

#### Installing
There is a package at https://bitbucket.org/twocanoes/virtual-serial-ccid-driver/downloads/ for installing /etc/reader.conf and the ifd-virtualserial.bundle CCID driver.

#### Logging
After installing the driver and config file, turn on logging:

log stream --debug --info --predicate '(subsystem == "com.apple.CryptoTokenKit")' --style syslog

###Trigger a virtual insertion event
The driver monitor for the existence for the file /tmp/scinsert. If the file is found, the driver triggers an insert event and CryptTokenKit drivers can vend the available certificates associated with identities to the system. 

In order to change certificates vended to the system, the file can be removed and then recreated to trigger removal and reinsertion. This will cause the CryptoTokenKit driver to be queried for new certificates.

 
`touch /tmp/scinsert`

`rmdir /tmp/scinsert`

### Using macOS tools
Once the driver is configured, a virtual insertion event is triggered and a CryptoTokenKit driver returns a valid certificate to the system, cryptographic operations can be done with standard macOS tools, such as:

`security list-smartcards`

`sc_auth  identities`

`codesign -f -s "Mac Developer: Catherine Clarkin (K4H5SYAW95)" /tmp/123`


### restart daemon
sudo killall -SIGKILL -m .*com.apple.ifdreader; sleep 1  && sudo launchctl unload /System/Library/LaunchDaemons/com.apple.ifdreader.plist && sudo launchctl load /System/Library/LaunchDaemons/com.apple.ifdreader.plist


